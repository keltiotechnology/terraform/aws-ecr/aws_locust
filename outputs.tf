output "public_subnet_ids" {
  value       = module.subnets.public_subnet_ids
  description = "The public subnet ids of VPC"
}

output "private_subnet_ids" {
  value       = module.subnets.private_subnet_ids
  description = "The private subnet ids of VPC"
}

output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "ID of VPC"
}

output "vpc_cidr_block" {
  value       = module.vpc.vpc_cidr_block
  description = "VPC cidr block"
}

output "vpc_default_security_group_id" {
  value       = module.vpc.vpc_default_security_group_id
  description = "The ID of the security group created by default on VPC creation"
}

output "instance_ip" {
  value       = aws_eip.web_instance_floating_ip.public_ip
  description = "Public ip of the locust instance"
}
